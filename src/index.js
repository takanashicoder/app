import 'core-js/es6/map';
import 'core-js/es6/set';
import React from 'react';
import ReactDOM from 'react-dom';
import '@vkontakte/vkui/dist/vkui.css';
import connect from '@vkontakte/vkui-connect';
import registerServiceWorker from './sw';
import LocationsLoader from "./LocationsLoader";
import App from './App';

// Init VK App
connect.send('VKWebAppInit', {});

// Service Worker For Cache
registerServiceWorker();

ReactDOM.render(<App locationsData={LocationsLoader.data} locationsTypes={LocationsLoader.types}/>, document.getElementById("root"));