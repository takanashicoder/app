import React from 'react';
import PropTypes from 'prop-types';
import {
    Checkbox,
    FormLayout,
    FormLayoutGroup,
    HeaderButton,
    HeaderContext,
    List,
    Panel,
    PanelHeader,
    PanelHeaderContent,
    Select
} from '@vkontakte/vkui';
import MainView from "../MainView";
import Icon28Settings from '@vkontakte/icons/dist/28/settings';
import Icon16Dropdown from '@vkontakte/icons/dist/24/dropdown';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            displayType: "map",
            locationsData: this.props.locationsData,
            contextOpened: false,
            mode: 'all',
        };
        this.checkboxes = {
            workNow: false,
            multicurrency: false,
            cashInsertion: false,
            payment: false
        };
        this.mode = "All";
        this.handler = this.handler.bind(this);
        this.toggleContext = this.toggleContext.bind(this);
        this.select = this.select.bind(this);

    }

    toggleContext() {
        this.setState({contextOpened: !this.state.contextOpened});
    }

    select(e) {
        const mode = e.currentTarget.dataset.mode;
        this.setState({mode});
        requestAnimationFrame(this.toggleContext);
    }

    handler(e) {
        e.preventDefault();
        this.setState({
            displayType: this.state.displayType === "list" ? "map" : "list",
            contextOpened: false
        });
    }

    recalc() {
        const filters = ["workNow", "multicurrency", "cashInsertion", "payment"];
        let currentDisplayType = this.state.displayType;
        this.setState({displayType: "spinner"});
        let locationsData = {};  //TODO: make all state manipulations reactive

        for (let places in this.props.locationsData) {
            if (this.mode !== "All" && places !== this.mode) continue;
            if (!this.props.locationsData.hasOwnProperty(places)) continue;
            let place = places.substring(0, places.length - 1);
            if (!this.props.locationsData[places].hasOwnProperty(place)) continue;
            locationsData[places] = {PageOffset: "0", TotalCount: 0};
            locationsData[places][place] = [];
            let placesData = this.props.locationsData[places][place];
            for (let placeData in placesData) {
                placeData = placesData[placeData];
                let flag = true;
                for (let filter in filters) {
                    filter = filters[filter];
                    if (placeData.hasOwnProperty(filter) &&
                        this.checkboxes[filter] !== placeData[filter]
                        && this.checkboxes[filter]) {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    locationsData[places][place].push(placeData);
                    locationsData[places].TotalCount++;
                }
            }
        }

        this.setState({
            displayType: currentDisplayType,
            locationsData: locationsData
        });
    }

    componentWillMount() {
        this.recalc();
    }

    render() {
        return (
            <Panel id="home">
                <PanelHeader
                    left={[<HeaderButton key="chng_btn"
                                         onClick={this.handler}><Icon28Settings/></HeaderButton>]}>
                    <PanelHeaderContent aside={<Icon16Dropdown/>} onClick={this.toggleContext}>
                        MasterCard Locations
                    </PanelHeaderContent>
                </PanelHeader>

                <HeaderContext opened={this.state.contextOpened} onClose={this.toggleContext}>
                    <List>
                        <FormLayout>
                            <Select top="Что вы хотите найти?" onChange={ (e) => {
                                this.mode = e.target.value;
                                this.recalc();
                            }}>
                                <option value="All" defaultChecked>АТМ, банки, магазины</option>
                                <option value="Atms">Банкоматы и устройства самообслуживания</option>
                                <option value="Banks">Банки</option>
                                <option value="Shops">"Наличные с покупкой"</option>
                            </Select>
                            <FormLayoutGroup top={"Фильтры"}>
                                <Checkbox onChange={(e) => {
                                    this.checkboxes.workNow = e.target.checked;
                                    this.recalc();
                                }} defaultChecked={this.checkboxes.workNow} >Работают сейчас</Checkbox>
                                <Checkbox onChange={(e) => {
                                    this.checkboxes.multicurrency = e.target.checked;
                                    this.recalc();
                                }} defaultChecked={this.checkboxes.multicurrency} >Мультивалютные</Checkbox>
                                <Checkbox onChange={(e) => {
                                    this.checkboxes.cashInsertion = e.target.checked;
                                    this.recalc();
                                }} defaultChecked={this.checkboxes.cashInsertion} >Внесение наличных </Checkbox>
                                <Checkbox onChange={(e) => {
                                    this.checkboxes.payment = e.target.checked;
                                    this.recalc();
                                }} defaultChecked={this.checkboxes.payment} >Оплата услуг</Checkbox>
                            </FormLayoutGroup>
                        </FormLayout>
                    </List>
                </HeaderContext>


                <div id={"mapCont"}>
                    <MainView displayType={this.state.displayType} locationsData={this.state.locationsData}
                              locationsTypes={this.props.locationsTypes}
                              fetchedGeoData={this.props.fetchedGeoData}/>
                </div>
            </Panel>
        )
    }
}

Home.propTypes = {
    id: PropTypes.string.isRequired,
    go: PropTypes.func.isRequired,
    locationsData: PropTypes.object,
    locationsTypes: PropTypes.object,
    fetchedUser: PropTypes.shape({
        photo_200: PropTypes.string,
        first_name: PropTypes.string,
        last_name: PropTypes.string,
        city: PropTypes.shape({
            title: PropTypes.string,
        }),
    }),
};

export default Home;
