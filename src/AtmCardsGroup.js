import React from 'react';
// import {Panel, ListItem, Button, Group, Div, Avatar, PanelHeader, View, Cell} from '@vkontakte/vkui';
import AdvancedAtmCard from "./AdvancedAtmCard";
const icon = require("./res/masterCard.png");

class AtmCardsGroup extends React.Component {
    render() {
        let atmCards = [];

        for (let places in this.props.locationsData) {
            if (!this.props.locationsData.hasOwnProperty(places)) continue;
            let place = places.substring(0, places.length - 1);
            if (!this.props.locationsData[places].hasOwnProperty(place)) continue;
            let placesData = this.props.locationsData[places][place];
            let cnt = 0;  //TODO
            for (let placeData in placesData) {
                if (cnt++ > 200)  //TODO
                    break;
                placeData = placesData[placeData];
                // console.log("PlaceData: ", placeData);
                atmCards.push(<AdvancedAtmCard key={placeData.Location.Name} icon={icon} location={placeData}/>);
            }
        }

        // this.props.locationsData.Atms.Atm.forEach(function (atmUnit) {
        //     atmCards.push(<AtmCard key={atmUnit.Location.Name} location={atmUnit.Location}
        //                            additionalData={atmUnit.data}/>);
        // });
        return (
            <div>
                {atmCards}
            </div>
        )
    }
}

export default AtmCardsGroup;

/*("Location": {
    "Name": "Sandbox ATM Location 1",
        "Distance": 0.93,
        "DistanceUnit": "MILE",
        "Address": {
        "Line1": "186 Asylum Avenue",
            "Line2": "",
            "City": "Stamford",
            "PostalCode": "11101",
            "CountrySubdivision": {
            "Name": "JACFGH",
                "Code": "GH"
        },
        "Country": {
            "Name": "Russia",
                "Code": "Russia"
        }
    },
    "Point": {
        "Latitude": 59.927649888795955,
            "Longitude": 30.302407467692806
    },
    "LocationType": {
        "Type": "OTHER"
    }
},
"HandicapAccessible": "NO",
    "Camera": "NO",
    "Availability": "UNKNOWN",
    "AccessFees": "UNKNOWN",
    "Owner": "Sandbox ATM 1",
    "SharedDeposit": "NO",
    "SurchargeFreeAlliance": "NO",
    "SurchargeFreeAllianceNetwork": "DOES_NOT_PARTICIPATE_IN_SFA",
    "Sponsor": "Sandbox",
    "SupportEMV": 1,
    "InternationalMaestroAccepted": 1*/